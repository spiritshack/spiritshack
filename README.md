We are fast becoming one of the largest Ghost Hunting Equipment stores in the UK. We dont have a shop front; our business is purely online based. This helps keep our costs down and gives you better prices. We started out reselling products from other manufacturers, and now design and build many of our own.

Website : https://www.spiritshack.co.uk/